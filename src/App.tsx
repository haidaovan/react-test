import { useState } from "react";
import Content from "./components/Content";

function App() {
  const [show, setShow] = useState(false);
  return (
    <>
      <button onClick={() => setShow(!show)}>Show</button>
      {show && <Content />}
    </>
  );
}

export default App;
