import { useEffect, useState } from "react";

const tabs = ["posts", "comments", "albums"];
const Content = () => {
  const [title, setTitle] = useState("");
  const [posts, setPosts] = useState([]);
  const [type, setType] = useState("");
  const [showGoToTop, setShowGoToTop] = useState(false);

  useEffect(() => {
    const callApi = async () => {
      try {
        const response = await fetch(
          `https://jsonplaceholder.typicode.com/${type}`
        );
        const posts = (await response.json()) as [];
        console.log("Fetch successful:");
        if (setPosts && typeof setPosts === "function") {
          setPosts(posts);
        }
      } catch (err) {
        console.error("Fetch error:", err);
      }
    };

    if (type) {
      callApi().catch((error) => console.error(error));
    } else {
      console.log("No API call");
    }
  }, [type, setPosts]);

  useEffect(() => {
    const handleSScroll = () => {
      console.log("scroll: ", window.scrollY);
      setShowGoToTop(window.scrollY >= 200);
    };
    window.addEventListener("scroll", handleSScroll);
    return () => {
      console.log("unmounting...");
      window.removeEventListener("scroll", handleSScroll);
    };
  }, []);
  // useEffect(() => {
  //   const callApi = async () => {
  //     await fetch(`https://jsonplaceholder.typicode.com/${type}`)
  //       .then((res) => res.json())
  //       .then((posts: []) => {
  //         console.log("ádfasdfasdf");

  //         setPosts(posts);
  //       })
  //       .catch((err) => console.log("àasfaf"));
  //   };
  //   console.log({ type });

  //   type ? callApi() : console.log("no call api");
  // }, [type]);

  return (
    <div>
      {tabs.map((tab) => (
        <button
          key={tab}
          onClick={() => setType(tab)}
          style={type === tab ? { color: "#fff", background: "#333" } : {}}
        >
          {tab}
        </button>
      ))}
      <input
        type="text"
        value={title}
        onChange={(e) => setTitle(e.target.value)}
      />
      {type && (
        <ul>
          {posts.map((post: { id: number; title?: string; name?: string }) => (
            <li key={post.id}>{post.title || post.name}</li>
          ))}
        </ul>
      )}
      {showGoToTop && (
        <button
          className="btn btn-primary"
          style={{ position: "fixed", right: 20, bottom: 20 }}
        >
          Go To Top
        </button>
      )}
    </div>
  );
};

export default Content;
